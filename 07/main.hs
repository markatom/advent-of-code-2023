#!/usr/bin/env stack
{- stack script --resolver lts-21.22 --package split,containers -}

import Data.List.Split (splitOn)
import Data.List (sort)
import Data.Map (Map, toList, delete, findWithDefault, insertWith, empty)

data Card =
  CX | C2 | C3 | C4 | C5 | C6 | C7 | C8 | C9 | CT | CJ | CQ | CK | CA
  deriving (Show, Eq, Ord)

parseCard :: Char -> Card
parseCard char = case char of
  'X' -> CX
  '2' -> C2
  '3' -> C3
  '4' -> C4
  '5' -> C5
  '6' -> C6
  '7' -> C7
  '8' -> C8
  '9' -> C9
  'T' -> CT
  'J' -> CJ
  'Q' -> CQ
  'K' -> CK
  'A' -> CA

data Type =
  HighCard | OnePair | TwoPairs | ThreeOfKind | FullHouse | FourOfKind | FiveOfKind
  deriving (Show, Eq, Ord)

determineType :: [Card] -> Type
determineType cards =
  case (jokersCount, sortedCountsWithoutJokers) of
    (5, _) -> FiveOfKind
    (4, _) -> FiveOfKind
    (3, 2:_) -> FiveOfKind
    (2, 3:_) -> FiveOfKind
    (1, 4:_) -> FiveOfKind
    (0, 5:_) -> FiveOfKind
    (3, _) -> FourOfKind
    (2, 2:_) -> FourOfKind
    (1, 3:_) -> FourOfKind
    (0, 4:_) -> FourOfKind
    (1, 2:2:_) -> FullHouse
    (0, 3:2:_) -> FullHouse
    (2, _) -> ThreeOfKind
    (1, 2:_) -> ThreeOfKind
    (0, 3:_) -> ThreeOfKind
    (0, 2:2:_) -> TwoPairs
    (1, _) -> OnePair
    (0, 2:_) -> OnePair
    _ -> HighCard
  where
    sortedCountsWithoutJokers = reverse $ sort $ snd <$> toList (delete CX cardToCountMap)
    jokersCount = findWithDefault 0 CX cardToCountMap
    cardToCountMap = foldl addCard empty cards
    addCard cardToCountMap card = insertWith (+) card 1 cardToCountMap

data Hand = Hand
  { hCards :: [Card]
  , hType :: Type
  } deriving (Show, Eq)

instance Ord Hand where
  (<=) (Hand hCards1 hType1) (Hand hCards2 hType2)
    | hType1 == hType2 = hCards1 <= hCards2
    | otherwise = hType1 <= hType2

parseHand :: String -> Hand
parseHand chars = Hand
  { hCards = cards
  , hType = determineType cards
  }
  where cards = parseCard <$> chars

data Jokers = WithoutJokers | WithJokers

parseLine :: Jokers -> String -> (Hand, Int)
parseLine jokers line =
  case splitOn " " line of
    [cardChars, stringBid] -> (parseHand (substituteJokers jokers <$> cardChars), read stringBid)
  where
    substituteJokers WithJokers 'J' = 'X'
    substituteJokers _ char = char

getTotalWinnings :: [(Hand, Int)] -> Int
getTotalWinnings handBid =
  sum $ uncurry (*) <$> (zip [1 ..] $ snd <$> sort handBid)

main :: IO ()
main = do
  content <- getContents
  print $ getTotalWinnings (parseLine WithoutJokers <$> lines content)
  print $ getTotalWinnings (parseLine WithJokers <$> lines content)

#!/usr/bin/env stack
{- stack script --resolver lts-21.22 --package split,containers -}

import Prelude hiding (lookup)
import Data.Char (isSpace)
import Data.List.Split (split, dropDelims, dropBlanks, oneOf)
import Data.Map (Map, fromList, toList, lookup)
import Data.Maybe (fromJust)

parseEdges :: [String] -> Map String (String, String)
parseEdges lines =
  fromList $ toKeyValuePairs . parseNode <$> lines
  where
    parseNode line =
      split (dropDelims . dropBlanks $ oneOf " ") $ removePunctuation <$> line
    removePunctuation char
      | not (isLabelChar char) && not (isSpace char) = ' '
      | otherwise = char
    isLabelChar char = 'A' <= char && char <= 'Z' || '0' <= char && char <= '9'
    toKeyValuePairs [from, left, right] = (from, (left, right))

getLength :: String -> (String -> Bool) -> String -> Map String (String, String) -> Int
getLength startNode isFinishNode path edges =
  go 0 startNode path
  where
    go steps node path
      | isFinishNode node = steps
      | otherwise = go (steps + 1) (advance path node) (tail path)
    advance (instruction:_) node =
      let (leftNode, rightNode) = fromJust $ lookup node edges
      in if instruction == 'L'
        then leftNode
        else rightNode

solve1 :: String -> Map String (String, String) -> Int
solve1 = getLength "AAA" $ (==) "ZZZ"

solve2 :: String -> Map String (String, String) -> Int
solve2 path edges =
  foldl lcm 1
    $ (\node -> getLength node isFinish path edges) <$> startNodes
  where
    startNodes = filter isStart $ fst <$> toList edges
    isStart = (==) 'A' . last
    isFinish = (==) 'Z' . last

main :: IO ()
main = do
  content <- getContents
  let path:_:edges = lines content
  print $ solve1 (concat $ repeat path) (parseEdges edges)
  print $ solve2 (concat $ repeat path) (parseEdges edges)

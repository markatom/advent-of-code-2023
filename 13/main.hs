#!/usr/bin/env stack
{- stack script --resolver lts-21.22 --package split -}

import Data.Foldable (asum)
import Data.List (findIndex, transpose)
import Data.List.Split (splitOn)
import Data.Maybe (fromMaybe, catMaybes)

alignLists :: ([a], [b]) -> ([a], [b])
alignLists (a, b) =
  (take finalLength $ reverse a, take finalLength b)
  where finalLength = min (length a) (length b)

difference :: ([String], [String]) -> Int
difference (a, b) =
  sum $ zipWith diffLine a b
  where
    diffLine a b = sum $ zipWith diffChar a b
    diffChar a b = if a == b then 0 else 1

findReflection :: Int -> [String] -> Maybe Int
findReflection requiredDifference pattern =
  (+1) <$> findIndex isReflection halves
  where
    halves = [alignLists $ splitAt i pattern | i <- [1 .. (length pattern) - 1]]
    isReflection = (==) requiredDifference . difference

summarize :: Int -> [[String]] -> Int
summarize requiredDifference patterns =
  sum $ verticalReflections ++ ((*100) <$> horizontalReflections)
  where
    verticalReflections = catMaybes [findReflection requiredDifference $ transpose pattern | pattern <- patterns]
    horizontalReflections = catMaybes [findReflection requiredDifference pattern | pattern <- patterns]

main :: IO ()
main = do
  content <- getContents
  let patterns = splitOn [""] $ lines content
  print $ summarize 0 patterns
  print $ summarize 1 patterns
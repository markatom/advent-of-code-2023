#!/usr/bin/env stack
{- stack script --resolver lts-21.22 -}

import Data.List (transpose, tails)
import Control.Monad (guard)

process :: Int -> [String] -> Int
process expansionMultiplier lines =
  sum $ distance <$> galaxyPairs
  where
    galaxies = do
      (row, chars) <- zip [0 ..] lines
      (column, char) <- zip [0 ..] chars
      guard $ char == '#'
      return (row, column)
    galaxyPairs = [(first, second) | (first:rest) <- tails galaxies, second <- rest]
    expanding source = do
      (index, chars) <- zip [0 ..] source
      guard $ all ((==) '.') chars
      return index
    expandingRows = expanding lines
    expandingColumns = expanding $ transpose lines
    distance ((row1, column1), (row2, column2))
      = (delta row1 row2)
      + (delta column1 column2)
      + (expansionMultiplier - 1) * (expansionLength row1 row2 expandingRows)
      + (expansionMultiplier - 1) * (expansionLength column1 column2 expandingColumns)
    delta a b = abs (a - b)
    expansionLength bound1 bound2 expanding =
      length $ filter (between bound1 bound2) expanding
    between bound1 bound2 subject
      | bound1 < bound2 = bound1 < subject && subject < bound2
      | otherwise = bound2 < subject && subject < bound1

main :: IO ()
main = do
  content <- getContents
  print $ process 2 $ lines content
  print $ process 1000000 $ lines content
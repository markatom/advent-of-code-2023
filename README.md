# Advent of Code 2023

My attempt to solve [Advent of Code 2023](https://adventofcode.com/2023) puzzles in Haskell.

## How to run

1. Install [The Haskell Tool Stack](https://docs.haskellstack.org/en/stable/install_and_upgrade/).

2. Navigate to the directory for the desired day and run the main script, passing the data via stdin. For example:

```
cd 01
cat input.txt | ./main.hs
```
#!/usr/bin/env stack
{- stack script --resolver lts-21.22 -}

extrapolate :: [Int] -> Int
extrapolate values =
  head $ foldl combine [0] $ drop 1 $ go values
  where
    combine (previous:rest) current = (current + previous):previous:rest
    go values
      | allZeros values = [0]
      | otherwise = go (zipWith (flip (-)) values $ tail values) ++ [last values]
    allZeros values = foldl (&&) True $ (== 0) <$> values

main :: IO ()
main = do
  content <- getContents
  print $ sum $ (extrapolate . (<$>) read . words) <$> lines content
  print $ sum $ (extrapolate . reverse . (<$>) read . words) <$> lines content
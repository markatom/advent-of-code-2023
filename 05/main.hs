#!/usr/bin/env stack
{- stack script --resolver lts-21.22 --package split -}

import Data.List.Split (splitOn, chunksOf)

type Interval = (Int, Int)

intersection :: Interval -> Interval -> [Interval]
intersection (start1, end1) (start2, end2)
  | end1 < start2 || end2 < start1 = []
  | otherwise = [(max start1 start2, min end1 end2)]

difference :: Interval -> Interval -> [Interval]
difference (start1, end1) (start2, end2) =
  leftPart ++ rightPart
  where
    leftPart
      | start1 < start2 = [(start1, min end1 start2)]
      | otherwise = []
    rightPart
      | end2 < end1 = [(max start1 end2, end1)]
      | otherwise = []

data MappingRule = MappingRule
  { source :: Interval
  , offset :: Int
  } deriving (Show)

data MappingResult = MappingResult
  { mapped :: [Interval]
  , unmapped :: [Interval]
  } deriving (Show)

mapValue :: MappingRule -> Interval -> MappingResult
mapValue (MappingRule { source, offset }) value =
  MappingResult
    { mapped = transpose <$> intersection value source
    , unmapped = difference value source
    }
  where
   transpose (start, end) = (start + offset, end + offset)

applyRules :: [MappingRule] -> Interval -> [Interval]
applyRules [] value = [value]
applyRules (rule:rules) value =
  case mapValue rule value of
    MappingResult { mapped, unmapped } ->
      mapped ++ (unmapped >>= applyRules rules)

applyCategories :: [Interval] -> [[MappingRule]] -> [Interval]
applyCategories values [] = values
applyCategories values (mappingRules:mappingCategories) =
  applyCategories (values >>= applyRules mappingRules) mappingCategories

parseRule :: String -> MappingRule
parseRule line =
  toMappingRule $ read <$> splitOn " " line
  where
    toMappingRule [destinationStart, sourceStart, length] = MappingRule
      { source = (sourceStart, sourceStart + length - 1)
      , offset = destinationStart - sourceStart
      }

parseCategories :: [[String]] -> [[MappingRule]]
parseCategories categorySections =
  parseCategory <$> categorySections
  where
    parseCategory categorySection = parseRule <$> drop 1 categorySection

type SeedParser = String -> [Interval]

parseSingleSeeds :: SeedParser
parseSingleSeeds line =
  toInterval . read
    <$> (drop 1 $ splitOn " " line)
  where
    toInterval start = (start, start)

parseSeedRanges :: SeedParser
parseSeedRanges line =
  toInterval <$> chunksOf 2 numbers
  where
    numbers = read <$> (drop 1 $ splitOn " " line)
    toInterval [start, length] = (start, start + length - 1)

getLowestLocation :: SeedParser -> [[String]] -> Int
getLowestLocation parseSeeds ([seedsSection]:categorySections) =
  foldl1 min $ fst <$> ranges
  where
    ranges = applyCategories (parseSeeds seedsSection) (parseCategories categorySections)

main :: IO ()
main = do
  content <- getContents
  let sections = splitOn [""] $ splitOn "\n" content
  print $ getLowestLocation parseSingleSeeds sections
  print $ getLowestLocation parseSeedRanges sections

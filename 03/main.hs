#!/usr/bin/env stack
{- stack script --resolver lts-21.22 --package split -}

import Control.Applicative ((<|>))
import Data.List.Split (split, dropBlanks, oneOf)
import Data.Maybe (fromJust)
import Data.Monoid (getAny, Any(Any))
import Text.Read (readMaybe)

data Number = Number
  { nValue :: Int
  , nX :: Int
  , nY :: Int
  } deriving (Show)

data Symbol = Symbol
  { sValue :: Char
  , sX :: Int
  , sY :: Int
  , sNumbersCount :: Int
  , sRatio :: Int
  } deriving (Show)

data Context = Context
  { cX :: Int
  , cY :: Int
  , cNumbers :: [Number]
  , cSymbols :: [Symbol]
  } deriving (Show)

type Parser = Context -> String -> Maybe Context

parseDots :: Parser
parseDots context chunk =
  if dotsCount == length chunk
    then Just $ context { cX = cX context + length chunk }
    else Nothing
  where dotsCount = length $ filter (== '.') chunk

parseNewline :: Parser
parseNewline context chunk =
  if chunk == "\n"
    then Just $ context
      { cX = 0
      , cY = cY context + 1
      }
    else Nothing

parseNumber :: Parser
parseNumber context chunk = do
  value <- readMaybe chunk
  return $ context
    { cX = cX context + length chunk
    , cNumbers = cNumbers context ++ [
      Number
        { nValue = value
        , nX = cX context
        , nY = cY context
        }
      ]
    }

parseSymbol :: Parser
parseSymbol context chunk = case chunk of
  [char] -> Just $ context
    { cX = cX context + 1
    , cSymbols = cSymbols context ++ [
      Symbol
        { sValue = char
        , sX = cX context
        , sY = cY context
        , sNumbersCount = 0
        , sRatio = 1
        }
      ]
    }
  _ -> Nothing

splitIntoChunks :: String -> [String]
splitIntoChunks content =
  splitByDots content >>= splitByNumbers
  where
    splitByDots = split (dropBlanks $ oneOf ".")
    splitByNumbers = split (dropBlanks $ oneOf ['0' .. '9'])

parse :: String -> Context
parse content =
  foldl processChunk initContext $ splitIntoChunks content
  where
    processChunk context chunk = fromJust
      $ parseDots context chunk
        <|> parseNewline context chunk
        <|> parseNumber context chunk
        <|> parseSymbol context chunk
    initContext = Context
      { cX = 0
      , cY = 0
      , cNumbers = []
      , cSymbols = []
      }

generateNeighbors :: Number -> [(Int, Int)]
generateNeighbors number =
  [(x, y) | x <- [left .. right], y <- [top .. bottom]]
  where
    left = nX number - 1
    right = nX number + length (show $ nValue number)
    top = nY number - 1
    bottom = nY number + 1

getPartNumbers :: Context -> [Int]
getPartNumbers context =
  nValue <$> filter isPartNumber (cNumbers context)
  where
    isPartNumber number =
      foldl (||) False
        $ hasSymbol
          <$> generateNeighbors number
    hasSymbol (x, y) =
      foldl (||) False
        $ (\symbol -> sX symbol == x && sY symbol == y)
          <$> cSymbols context

calculateRatios :: Context -> Context
calculateRatios context =
  context { cSymbols = cSymbols $ go context}
  where
    go context = case context of
      Context { cNumbers = [] } -> context
      Context { cNumbers = number:numbers } -> go $ context
        { cSymbols = processSymbol number <$> cSymbols context
        , cNumbers = numbers
        }
    processSymbol number symbol =
      if areNeighbors number symbol
        then symbol
          { sNumbersCount = sNumbersCount symbol + 1
          , sRatio = sRatio symbol * (nValue number)
          }
        else symbol
    areNeighbors number symbol =
      foldl (||) False
        $ (== (sX symbol, sY symbol))
          <$> generateNeighbors number

getRatios :: Context -> [Int]
getRatios context =
  sRatio <$> filter ((== 2) . sNumbersCount) (cSymbols context)

main = do
  content <- getContents
  let context = parse content
  print $ sum $ getPartNumbers context
  print $ sum $ getRatios $ calculateRatios context

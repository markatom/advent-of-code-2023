#!/usr/bin/env stack
{- stack script --resolver lts-21.22 -}

import Data.Foldable (asum)
import Data.List (inits, tails, isPrefixOf, isSuffixOf)
import Data.Maybe (fromMaybe)

digits = numeralDigits ++ spelledDigits
  where
    numeralDigits = numeralDigit <$> [1 .. 9]
    numeralDigit n = (show n, n)
    spelledDigits =
      [ ("one", 1)
      , ("two", 2)
      , ("three", 3)
      , ("four", 4)
      , ("five", 5)
      , ("six", 6)
      , ("seven", 7)
      , ("eight", 8)
      , ("nine", 9)
      ]

getDigit sequences matcher line =
  fromMaybe 0 $ asum $ processDigits <$> sequences line
  where
    processDigits subject =
      asum $ matchDigit subject <$> digits
    matchDigit subject (search, result) =
      if matcher search subject
        then Just result
        else Nothing

main = do
  content <- getContents
  print $ sum $ getCalibrationValue <$> lines content
  where
    getCalibrationValue line = 10 * getFirstDigit line + getSecondDigit line
    getFirstDigit = getDigit tails isPrefixOf
    getSecondDigit = getDigit (reverse . inits) isSuffixOf

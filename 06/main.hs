#!/usr/bin/env stack
{- stack script --resolver lts-21.22 --package split -}

import Data.List.Split (split, dropDelims, dropBlanks, oneOf)

getGreaterInt :: Double -> Int
getGreaterInt n
  | (fromIntegral $ ceiling n) == n = ceiling n + 1
  | otherwise = ceiling n

getLesserInt :: Double -> Int
getLesserInt n
  | (fromIntegral $ floor n) == n = floor n - 1
  | otherwise = floor n

solveQuadraticEquation :: (Double, Double, Double) -> (Double, Double)
solveQuadraticEquation (a, b, c) =
  (x1, x2)
  where
    d = b ^ 2 - 4.0 * a * c
    x1 = (-b - sqrt d) / 2.0
    x2 = (-b + sqrt d) / 2.0

calculate :: (Int, Int) -> Int
calculate (time, distance) =
  (getLesserInt upperBound) - (getGreaterInt lowerBound) + 1
  where (lowerBound, upperBound) = solveQuadraticEquation (-1, fromIntegral time, - fromIntegral distance)

parseLine :: String -> [Int]
parseLine line =
  read <$> numbers
  where numbers = drop 1 $ split (dropDelims . dropBlanks $ oneOf " ") line

joinNumbers :: [Int] -> Int
joinNumbers numbers =
  read $ foldl (++) "" $ show <$> numbers

main :: IO ()
main = do
  content <- getContents
  let [timelines, distances] = parseLine <$> lines content
  print $ foldl (*) 1 $ calculate <$> zip timelines distances
  print $ calculate $ (joinNumbers timelines, joinNumbers distances)

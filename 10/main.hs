#!/usr/bin/env stack
{- stack script --resolver lts-21.22 --package containers -}

import Data.Set (Set, fromList, insert, size, member, notMember)
import Data.Foldable (asum)
import Data.Maybe (fromJust, fromMaybe, catMaybes)

data Direction =
  North | South | West | East
  deriving (Show, Eq)

type Pipe = [Direction]

type Position = (Int, Int)

parsePipe :: Char -> Pipe
parsePipe char =
  case char of
    'S' -> [North, South, West, East]
    '|' -> [North, South]
    '-' -> [West, East]
    'L' -> [North, East]
    'J' -> [North, West]
    '7' -> [South, West]
    'F' -> [South, East]
    '.' -> []

opposite :: Direction -> Direction
opposite direction =
  case direction of
    North -> South
    South -> North
    West -> East
    East -> West

getSemiPipe :: Direction -> Pipe -> Maybe Pipe
getSemiPipe direction pipe =
  if pipe /= semiPipe
    then Just semiPipe
    else Nothing
  where
    semiPipe = filter notOppositeDirection pipe
    notOppositeDirection =
      (/=) (opposite direction)

getStart :: String -> Position
getStart content =
  fromJust $ asum $ zipWith outer [0 ..] $ lines content
  where
    outer row line =
      case asum $ zipWith inner [0 ..] line of
        Just column -> Just (row, column)
        _ -> Nothing
    inner column 'S' = Just column
    inner _ _ = Nothing

advancePosition :: Direction -> Position -> Position
advancePosition direction (row, column) =
  case direction of
    North -> (row - 1, column)
    South -> (row + 1, column)
    West -> (row, column - 1)
    East -> (row, column + 1)

atPosition :: [[Pipe]] -> Position -> Maybe Pipe
atPosition pipes (row, column) = do
  columns <- atIndex pipes row
  atIndex columns column
  where
    atIndex list index
      | index >= 0 && index < length list = Just $ list !! index
      | otherwise = Nothing

next :: [[Pipe]] -> Position -> Direction -> Maybe (Position, Pipe)
next pipes position direction = do
  pipe <- atPosition pipes nextPosition
  semiPipe <- getSemiPipe direction pipe
  return (nextPosition, semiPipe)
  where nextPosition = advancePosition direction position

getLoop :: Position -> [[Pipe]] -> Set Position
getLoop start pipes =
  let [next1, next2] = catMaybes $ next pipes start <$> [North, South, West, East]
  in go (fromList [start, fst next1, fst next2]) next1 next2
  where
    go loop (position1, semiPipe1) (position2, semiPipe2) =
      if (fst next1) /= (fst next2)
        then
          go (insert (fst next1) (insert (fst next2) loop)) next1 next2
        else
          insert (fst next1) loop
      where
        next1 = fromJust $ asum $ next pipes position1 <$> semiPipe1
        next2 = fromJust $ asum $ next pipes position2 <$> semiPipe2

countEnclosed :: [[Pipe]] -> Set Position -> Int
countEnclosed pipes loop =
  sum $ [go (row, 0) False 0 | row <- [0 .. (length pipes) - 1]]
  where
    go (row, column) inside count
      | column >= length (pipes !! row) = count
      | otherwise = go (row, column + 1) nextInside nextCount
      where
        nextInside =
          if member (row, column) loop && fromMaybe False isVerticalConnection
            then not inside
            else inside
        nextCount =
          if inside && notMember (row, column) loop
            then count + 1
            else count
        isVerticalConnection = do
          upperPipe <- atPosition pipes (row, column)
          lowerPipe <- atPosition pipes (row + 1, column)
          return $ elem South upperPipe && elem North lowerPipe

main :: IO ()
main = do
  content <- getContents
  let pipes = (<$>) parsePipe <$> lines content
  let loop = getLoop (getStart content) pipes
  print $ flip div 2 $ size $ loop
  print $ countEnclosed pipes loop
#!/usr/bin/env stack
{- stack script --resolver lts-21.22 --package split -}

import Data.Char (isSpace)
import Data.List (dropWhileEnd, dropWhile)
import Data.List.Split (splitOn)
import Data.Maybe (fromMaybe)

trimSplitOn :: String -> String -> [String]
trimSplitOn delimiter subject =
  trim <$> splitOn delimiter subject
  where trim = dropWhile isSpace . dropWhileEnd isSpace

data Bag = Bag
  { red :: Int
  , green :: Int
  , blue :: Int
  } deriving (Eq, Show)

instance Num Bag where
  Bag r1 g1 b1 + Bag r2 g2 b2 =
    Bag (r1 + r2) (g1 + g2) (b1 + b2)

instance Ord Bag where
  max (Bag r1 g1 b1) (Bag r2 g2 b2) =
    Bag (max r1 r2) (max g1 g2) (max b1 b2)

emptyBag = Bag 0 0 0

parseBag :: String -> Bag
parseBag input = sum $ parse <$> trimSplitOn "," input
  where
    parse item =
      case trimSplitOn " " item of
        [count, "red"] -> emptyBag { red = read count }
        [count, "green"] -> emptyBag { green = read count }
        [count, "blue"] -> emptyBag { blue = read count }
    sum = foldl (+) emptyBag

getPossibleGameId :: String -> Maybe Int
getPossibleGameId input =
  case trimSplitOn ":" input of
    [header, bags] ->
      if validateBags bags
        then Just $ parseHeader header
        else Nothing
  where
    validateBags bags =
      foldl (&&) True
        $ validateBag . parseBag <$> trimSplitOn ";" bags
    validateBag (Bag r g b) =
      r <= 12 && g <= 13 && b <= 14
    parseHeader header =
      case trimSplitOn " " header of
        ["Game", id] -> read id

getGamePower :: String -> Int
getGamePower input =
  case trimSplitOn ":" input of
    [_, bags] -> power $ minBag bags
  where
    power (Bag r g b) = r * g * b
    minBag bags =
      foldl (max) emptyBag
        $ parseBag <$> trimSplitOn ";" bags

main = do
  content <- getContents
  print $ sum $ fromMaybe 0 . getPossibleGameId <$> lines content
  print $ sum $ getGamePower <$> lines content
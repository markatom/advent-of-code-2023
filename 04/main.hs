#!/usr/bin/env stack
{- stack script --resolver lts-21.22 --package split,containers -}

import Data.Char (isSpace)
import Data.List (dropWhileEnd, dropWhile)
import Data.List.Split (split, dropDelims, dropBlanks, oneOf)
import Data.Set (fromList, intersection, size)

trimSplitOn :: String -> String -> [String]
trimSplitOn delimiter subject =
  trim <$> split splitter subject
  where
    splitter = dropBlanks . dropDelims $ oneOf delimiter
    trim = dropWhile isSpace . dropWhileEnd isSpace

getCardMatches :: String -> Int
getCardMatches line =
  case trimSplitOn ":" line of
    [_, numbers] ->
      case trimSplitOn "|" numbers  of
        [winningNumbers, ourNumbers] ->
          size $ intersection (createSet winningNumbers) (createSet ourNumbers)
  where
    createSet numbers =
      fromList $ (read :: String -> Int) <$> trimSplitOn " " numbers

calculatePoints :: Int -> Int
calculatePoints matches
  | matches == 0 = 0
  | otherwise = 2 ^ (matches - 1)

countDerivedCards :: [Int] -> Int
countDerivedCards cards =
  go 0 $ (, 1) <$> cards
  where
    go totalCount [] = totalCount
    go totalCount ((matches, count):restCards) =
      go (totalCount + count)
        $ (addCount count <$> take matches restCards) ++ drop matches restCards
    addCount toAdd (matches, count) = (matches, count + toAdd)

main :: IO ()
main = do
  content <- getContents
  print $ sum $ calculatePoints . getCardMatches <$> lines content
  print $ countDerivedCards $ getCardMatches <$> lines content

#!/usr/bin/env stack
{- stack script --resolver lts-21.22 --package split,containers -}

import Data.List (intercalate)
import Data.List.Split (splitOn)
import Data.Map (fromList, fromListWith, toList, findWithDefault)

type State = (Int, Int)

next :: State -> Char -> [Int] -> [State]
next (0, 0) '.' _ = [(0, 0)]
next (0, 0) '?' _ = [(0, 0)]
next (0, 0) '#' _ = []
next (position, 0) '.' rules = [(position - 1, rules !! (position - 1))]
next (position, 0) '?' rules = [(position - 1, rules !! (position - 1))]
next (position, 0) '#' _ = []
next (position, count) '.' rules
  | (rules !! position) == count = [(position, count)]
  | otherwise = []
next (position, count) '?' rules
  | (rules !! position) == count = [(position, count), (position, count - 1)]
  | otherwise = [(position, count - 1)]
next (position, count) '#' rules = [(position, count - 1)]

solve :: String -> [Int] -> Int
solve input rules =
  go input $ fromList [(((length rules) - 1, last rules), 1)]
  where
    go "" states = findWithDefault 0 (0, 0) states
    go (input:rest) states =
      go rest $ fromListWith (+) $ do
        state <- toList states
        nextWithOccurrences state input rules
    nextWithOccurrences (state, occurrences) input rules = do
      newState <- next state input rules
      return (newState, occurrences)

main :: IO ()
main = do
  content <- getContents
  let assignments = parseLine . splitOn " " <$> lines content
  print $ sum $ uncurry solve <$> assignments
  print $ sum $ uncurry solve2 <$> assignments
  where
    solve2 input rules = solve (unfold "?" input) (unfold [] rules)
    parseLine [input, rules] = (input, reverse $ read <$> splitOn "," rules)
    unfold delimiter = intercalate delimiter . replicate 5